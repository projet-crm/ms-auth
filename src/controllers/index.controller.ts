import { NextFunction, Request, Response } from 'express';
import jsonResponse from '@/mixins/jsonResponse.mixin';
class IndexController extends jsonResponse {
  public index = (req: Request, res: Response, next: NextFunction): void => {
    try {
      this.ok(res);
    } catch (error) {
      next(error);
    }
  };
}

export default IndexController;
