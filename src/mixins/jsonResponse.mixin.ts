import { Response } from 'express';

class jsonResponse {
  public jsonResponse(res: Response, code: number, message: string) {
    const payload = {
      status: code,
      time: new Date(),
      message,
    };
    return res.status(code).json(payload);
  }

  public ok<T>(res: Response, dto?: T) {
    if (dto) {
      const payload = {
        status: 200,
        time: new Date(),
        datas: dto,
      };
      return res.status(200).json(payload);
    }
    return res.sendStatus(200);
  }

  public created<T>(res: Response, dto?: T) {
    if (dto) {
      const payload = {
        status: 201,
        time: new Date(),
        datas: dto,
      };
      return res.status(201).json(payload);
    }
    return res.sendStatus(201);
  }

  public clientError(res: Response, message?: string) {
    return this.jsonResponse(res, 400, message || 'Unauthorized');
  }

  public unauthorized(res: Response, message?: string) {
    return this.jsonResponse(res, 401, message || 'Unauthorized');
  }

  public forbidden(res: Response, message?: string) {
    return this.jsonResponse(res, 403, message || 'Forbidden');
  }

  public notFound(res: Response, message?: string) {
    return this.jsonResponse(res, 404, message || 'Not found');
  }

  public conflict(res: Response, message?: string) {
    return this.jsonResponse(res, 409, message || 'Conflict');
  }

  public tooMany(res: Response, message?: string) {
    return this.jsonResponse(res, 429, message || 'Too many requests');
  }

  public todo(res: Response) {
    return this.jsonResponse(res, 400, 'TODO');
  }

  public fail(res: Response, error: Error | string) {
    console.log(error);
    return res.status(500).json({
      message: error.toString(),
    });
  }

  public getterOk<T>(res: Response, dto?: T) {
    if (dto) {
      const payload = {
        status: 200,
        time: new Date(),
        ...dto,
      };
      return res.status(200).json(payload);
    }
    return res.sendStatus(200);
  }
}

export default jsonResponse;
